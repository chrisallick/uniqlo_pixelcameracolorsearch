// alternative behance api
//var url = "http://www.behance.net/v2/projects?color_hex=A4A9B0&api_key=Azjbs5sKUAqkkXvmwiTnUsN78nPhVizt"

function componentToHex(c) {
    var hex = c.toString(16);
    return hex.length == 1 ? "0" + hex : hex;
}

function rgbToHex(r, g, b) {
    return "" + componentToHex(r) + componentToHex(g) + componentToHex(b);
}

var getImages = function( img_data ) {
	clearTimeout( t );

	$("#images").html("");

	/*
		just get top right color
		so you don't smash API
	*/
	// for(var y = 0; y < h; y++) {
	// 	for(var x = 0; x < w; x++) {
			r = img_data[0];
			g = img_data[1];
			b = img_data[2];

			var hex = rgbToHex(r,g,b);
	// 	}
	// }

	var url_2 = "http://labs.tineye.com/rest/?method=flickr_color_search&limit=28&offset=0&colors%5B0%5D="+hex+"&colors%5B1%5D=429553&weights%5B0%5D=0.9&weights%5B1%5D=0.1";
	var url_3 = "http://labs.tineye.com/rest/?method=flickr_color_search&limit=28&offset=0&colors%5B0%5D="+hex+"&weights%5B0%5D=1";
	
	$.ajax({
		type: 'GET',
	    url: url_3,
	    async: false,
	    jsonpCallback: 'jsonCallback',
	    contentType: "application/json",
	    dataType: 'jsonp',
	    success: function(json) {
	    	console.log( json );
	    	if( json.result ) {
	    		var to_load = json.result.length;
	    		var loaded = 0;
	     		for( var i = 0, len = json.result.length; i < len; i++ ) {
	     			if( json.result[i].filepath ) {
	     				console.log( json.result[i].filepath );
	     				var path = "http://img.tineye.com/flickr-images/?filepath=labs-flickr/"+json.result[i].filepath+"&size=100";
	     				var img = new Image();
	     				img.onload = function() {
	     					var i = this;

	     					$(i).css({
	     						width: "100%",
	     						height: "100%",
	     						display: "block",
	     						zIndex: 1
	     					});

	     					var overlay = document.createElement("img");
	     					overlay.src = "./img/logo.png";
	     					$(overlay).css({
	     						width: "100px",
	     						height: "100px",
	     						display: "block",
	     						position: "absolute",
	     						top: 0,
	     						left: 0,
	     						zIndex: 10
	     					});

	     					var o = document.createElement("div");
	     					o.className = "thumb";
	     					$(o).append(i).append(overlay).appendTo($("#images"));
	     					
	     					loaded++;
	     					if( loaded == to_load ) {
								t = setTimeout( function() {
									getimages = true;
								}, 5000 );
	     					}
	     				}
	     				img.error = function() {
	     					to_load--;
	     				}
	     				img.src = path;
	     			}
	     		}
	    	}
	    },
	    error: function(e) {
			console.log(e.message);
	    }
	});
}

var t, getimages = false;
$(document).ready(function() {
		var isStreaming = false,
			v = document.getElementById('v'),
			c = document.getElementById('c'),
			c2 = document.getElementById('c2'),
			grey = document.getElementById('grey'),
			con = c.getContext('2d'),
			con2 = c2.getContext('2d'),
			w = 5,
			h = 5,
			greyscale = false,
			r = 0,
			g = 0,
			b = 0;

		// Cross browser
		navigator.getUserMedia = (navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia);
		if (navigator.getUserMedia) {
			// Request access to video only
			navigator.getUserMedia(
				{
					video:true,
					audio:false
				},		
				function(stream) {
					// Cross browser checks
					var url = window.URL || window.webkitURL;
        			v.src = url ? url.createObjectURL(stream) : stream;
        			// Set the video to play
        			v.play();
				},
				function(error) {
					alert('Something went wrong. (error code ' + error.code + ')');
					return;
				}
			);
		}
		else {
			alert('Sorry, the browser you are using doesn\'t support getUserMedia');
			return;
		}

		// Wait until the video stream can play
		v.addEventListener('canplay', function(e) {
		    if (!isStreaming) {
		    	// videoWidth isn't always set correctly in all browsers
//		    	if (v.videoWidth > 0) h = v.videoHeight / (v.videoWidth / w);

				c.setAttribute('width', w);
				c.setAttribute('height', h);
				
				c2.setAttribute('width', 500);
				c2.setAttribute('height', 500);
				// Reverse the canvas image
				con.translate(w, 0);
  				con.scale(-1, 1);
  				
  				con2.translate(500, 0);
  				con2.scale(-1,1);
		      	
		      	isStreaming = true;
		    }
		}, false);

		// Wait for the video to start to play
		v.addEventListener('play', function() {
			setInterval(function() {
				if (v.paused || v.ended) return;
				con.fillRect(0, 0, w, h);
				con.drawImage(v, 0, 0, w, h);

				var imageData = con.getImageData(0, 0, w, h);
				var data = imageData.data;

        		for(var y = 0; y < h; y++) {
          			for(var x = 0; x < w; x++) {
            			r = data[((w * y) + x) * 4];
            			g = data[((w * y) + x) * 4 + 1];
            			b = data[((w * y) + x) * 4 + 2];
            			a = data[((w * y) + x) * 4 + 3];

  						con2.fillStyle = "rgba("+r+", "+g+", "+b+", 1)";
  						con2.fillRect(x*100, y*100, 100, 100);
            		}
            	}

            	if( getimages ) {
            		getimages = false;
            		getImages( data );
            	}

			}, 33);
		}, false);

	t = setTimeout( function(){
		getimages = true;
	}, 5000 );
});